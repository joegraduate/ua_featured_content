# UA Featured Content* (Carousel) Drupal Feature Module

(*) Name subject to change.

Provides content type and view for Featured Content Carousel component consistent with UA brand strategy.

## Features

- Provides 'featured_content' content type.
- Provides carousel view for featured_content items.
- Provides 'ua_hero_carousel' Flexslider preset.

## Packaged Dependencies

When this module is used as part of a Drupal distribution (such as [UA Quickstart](https://bitbucket.org/ua_drupal/ua_quickstart)), the following dependencies will be automatically packaged with the distribution.

### Drupal Contrib Modules

- [Flexslider](https://www.drupal.org/project/flexslider)
- [Image URL Formatter](https://www.drupal.org/project/image_url_formatter)

### Libraries

- [Flexslider](http://www.woothemes.com/flexslider/)
